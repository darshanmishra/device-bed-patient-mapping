-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2017 at 01:00 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_beds`
--

CREATE TABLE `rc_hospital_beds` (
  `hospital_bed_id` double NOT NULL,
  `umbrella` double NOT NULL,
  `hospital` double NOT NULL,
  `hospital_location` double NOT NULL,
  `hospital_location_area` double NOT NULL,
  `bed_number` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `created_by` double NOT NULL,
  `modified_by` double NOT NULL,
  `login_userid` double NOT NULL,
  `login_password` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='List of beds in the hospital\r\nThis is the final place for the key settings';

--
-- Dumping data for table `rc_hospital_beds`
--

INSERT INTO `rc_hospital_beds` (`hospital_bed_id`, `umbrella`, `hospital`, `hospital_location`, `hospital_location_area`, `bed_number`, `created_on`, `modified_on`, `created_by`, `modified_by`, `login_userid`, `login_password`) VALUES
(1, 1, 1, 1, 2, '408', '2017-07-14 17:31:36', '2017-07-14 17:31:36', 0, 0, 0, 0),
(2, 1, 1, 1, 2, '409', '2017-07-14 17:31:43', '2017-07-14 17:31:43', 0, 0, 0, 0),
(3, 1, 1, 1, 3, '401A', '2017-07-14 17:31:57', '2017-07-14 17:31:57', 0, 0, 0, 0),
(4, 1, 1, 1, 3, '401B', '2017-07-14 17:32:06', '2017-07-14 17:32:06', 0, 0, 0, 0),
(5, 1, 1, 1, 4, '402A', '2017-07-14 17:32:25', '2017-07-14 17:32:25', 0, 0, 0, 0),
(6, 1, 1, 1, 5, '410A', '2017-07-14 17:32:41', '2017-07-14 17:32:41', 0, 0, 0, 0),
(7, 1, 1, 1, 5, '410B', '2017-07-14 17:32:51', '2017-07-14 17:32:51', 0, 0, 0, 0),
(8, 2, 2, 3, 9, '101/1', '2017-07-20 16:46:37', '2017-07-20 16:46:37', 0, 0, 0, 0),
(9, 2, 2, 3, 9, '102/1', '2017-07-20 16:46:55', '2017-07-20 16:46:55', 0, 0, 0, 0),
(10, 2, 2, 3, 9, '102/2', '2017-07-20 16:47:02', '2017-07-20 16:47:02', 0, 0, 0, 0),
(11, 2, 2, 3, 9, '103/1', '2017-07-20 16:47:27', '2017-07-20 16:47:27', 0, 0, 0, 0),
(12, 2, 2, 3, 9, '103/2', '2017-07-20 16:47:33', '2017-07-20 16:47:33', 0, 0, 0, 0),
(13, 2, 2, 3, 9, '104/1', '2017-07-20 16:47:45', '2017-07-20 16:47:45', 0, 0, 0, 0),
(14, 2, 2, 3, 9, '104/2', '2017-07-20 16:47:52', '2017-07-20 16:47:52', 0, 0, 0, 0),
(15, 2, 2, 3, 9, '105/1', '2017-07-20 16:48:02', '2017-07-20 16:48:02', 0, 0, 0, 0),
(16, 2, 2, 3, 9, '105/2', '2017-07-20 16:48:09', '2017-07-20 16:48:09', 0, 0, 0, 0),
(17, 2, 2, 3, 9, '106/1', '2017-07-20 16:48:21', '2017-07-20 16:48:21', 0, 0, 0, 0),
(18, 2, 2, 3, 14, '201', '2017-07-21 10:39:51', '2017-07-21 10:39:51', 0, 0, 0, 0),
(19, 2, 2, 3, 14, '202', '2017-07-21 10:39:56', '2017-07-21 10:39:56', 0, 0, 0, 0),
(20, 2, 2, 3, 14, '203/1', '2017-07-21 10:40:12', '2017-07-21 10:40:12', 0, 0, 0, 0),
(21, 2, 2, 3, 14, '203/2', '2017-07-21 10:40:17', '2017-07-21 10:40:17', 0, 0, 0, 0),
(22, 2, 2, 3, 14, '204/1', '2017-07-21 10:40:22', '2017-07-21 10:40:22', 0, 0, 0, 0),
(23, 2, 2, 3, 14, '204/2', '2017-07-21 10:40:27', '2017-07-21 10:40:27', 0, 0, 0, 0),
(24, 2, 2, 3, 14, '205/1', '2017-07-21 10:40:46', '2017-07-21 10:40:46', 0, 0, 0, 0),
(25, 2, 2, 3, 14, '205/2', '2017-07-21 10:40:51', '2017-07-21 10:40:51', 0, 0, 0, 0),
(26, 2, 2, 3, 14, '206/1', '2017-07-21 10:40:56', '2017-07-21 10:40:56', 0, 0, 0, 0),
(27, 2, 2, 3, 14, '206/2', '2017-07-21 10:41:02', '2017-07-21 10:41:02', 0, 0, 0, 0),
(28, 2, 2, 3, 9, '106/2', '2017-07-21 11:16:31', '2017-07-21 11:16:31', 0, 0, 0, 0),
(29, 2, 2, 3, 9, '106/7', '2017-07-21 11:16:37', '2017-07-21 11:16:37', 0, 0, 0, 0),
(30, 2, 2, 3, 12, '108/1', '2017-07-21 11:16:59', '2017-07-21 11:16:59', 0, 0, 0, 0),
(31, 2, 2, 3, 12, '108/2', '2017-07-21 11:17:05', '2017-07-21 11:17:05', 0, 0, 0, 0),
(32, 2, 2, 3, 12, '109/1', '2017-07-21 11:17:14', '2017-07-21 11:17:14', 0, 0, 0, 0),
(33, 2, 2, 3, 12, '109/2', '2017-07-21 11:17:19', '2017-07-21 11:17:19', 0, 0, 0, 0),
(34, 2, 2, 3, 12, '110/1', '2017-07-21 11:17:25', '2017-07-21 11:17:25', 0, 0, 0, 0),
(35, 2, 2, 3, 12, '110/2', '2017-07-21 11:17:29', '2017-07-21 11:17:29', 0, 0, 0, 0),
(36, 2, 2, 3, 12, '110/3', '2017-07-21 11:17:34', '2017-07-21 11:17:34', 0, 0, 0, 0),
(37, 2, 2, 3, 12, '110/4', '2017-07-21 11:17:39', '2017-07-21 11:17:39', 0, 0, 0, 0),
(38, 2, 2, 3, 12, '111', '2017-07-21 11:17:56', '2017-07-21 11:17:56', 0, 0, 0, 0),
(39, 2, 2, 3, 12, '112', '2017-07-21 11:17:58', '2017-07-21 11:17:58', 0, 0, 0, 0),
(40, 2, 2, 3, 12, '114', '2017-07-21 11:18:02', '2017-07-21 11:18:02', 0, 0, 0, 0),
(41, 2, 2, 3, 12, '115', '2017-07-21 11:18:05', '2017-07-21 11:18:05', 0, 0, 0, 0),
(42, 2, 2, 3, 12, '116', '2017-07-21 11:18:08', '2017-07-21 11:18:08', 0, 0, 0, 0),
(43, 2, 2, 3, 12, '117', '2017-07-21 11:18:11', '2017-07-21 11:18:11', 0, 0, 0, 0),
(44, 2, 2, 3, 12, '118', '2017-07-21 11:18:17', '2017-07-21 11:18:17', 0, 0, 0, 0),
(45, 2, 2, 3, 13, '119', '2017-07-21 11:18:40', '2017-07-21 11:18:40', 0, 0, 0, 0),
(46, 2, 2, 3, 13, '120', '2017-07-21 11:18:44', '2017-07-21 11:18:44', 0, 0, 0, 0),
(47, 2, 2, 3, 13, '121', '2017-07-21 11:18:47', '2017-07-21 11:18:47', 0, 0, 0, 0),
(48, 2, 2, 3, 13, '122', '2017-07-21 11:18:50', '2017-07-21 11:18:50', 0, 0, 0, 0),
(49, 2, 2, 3, 13, '123', '2017-07-21 11:18:52', '2017-07-21 11:18:52', 0, 0, 0, 0),
(50, 2, 2, 3, 13, '124', '2017-07-21 11:18:57', '2017-07-21 11:18:57', 0, 0, 0, 0),
(51, 2, 2, 3, 13, '125', '2017-07-21 11:19:00', '2017-07-21 11:19:00', 0, 0, 0, 0),
(52, 2, 2, 3, 13, '126', '2017-07-21 11:19:07', '2017-07-21 11:19:07', 0, 0, 0, 0),
(53, 2, 2, 3, 13, '127', '2017-07-21 11:19:11', '2017-07-21 11:19:11', 0, 0, 0, 0),
(54, 2, 2, 3, 13, '128', '2017-07-21 11:19:15', '2017-07-21 11:19:15', 0, 0, 0, 0),
(55, 2, 2, 3, 13, '129', '2017-07-21 11:19:21', '2017-07-21 11:19:21', 0, 0, 0, 0),
(56, 2, 2, 3, 14, '207/1', '2017-07-21 11:19:53', '2017-07-21 11:19:53', 0, 0, 0, 0),
(57, 2, 2, 3, 14, '207/2', '2017-07-21 11:19:58', '2017-07-21 11:19:58', 0, 0, 0, 0),
(58, 2, 2, 3, 14, '208/1', '2017-07-21 11:20:04', '2017-07-21 11:20:04', 0, 0, 0, 0),
(59, 2, 2, 3, 14, '208/2', '2017-07-21 11:20:10', '2017-07-21 11:20:10', 0, 0, 0, 0),
(60, 2, 2, 3, 15, '209', '2017-07-21 11:21:29', '2017-07-21 11:21:29', 0, 0, 0, 0),
(61, 2, 2, 3, 15, '210', '2017-07-21 11:21:34', '2017-07-21 11:21:34', 0, 0, 0, 0),
(62, 2, 2, 3, 15, '211', '2017-07-21 11:21:38', '2017-07-21 11:21:38', 0, 0, 0, 0),
(63, 2, 2, 3, 15, '212', '2017-07-21 11:21:41', '2017-07-21 11:21:41', 0, 0, 0, 0),
(64, 2, 2, 3, 15, '213', '2017-07-21 11:21:46', '2017-07-21 11:21:46', 0, 0, 0, 0),
(65, 2, 2, 3, 15, '214', '2017-07-21 11:21:49', '2017-07-21 11:21:49', 0, 0, 0, 0),
(66, 2, 2, 3, 15, '215', '2017-07-21 11:21:55', '2017-07-21 11:21:55', 0, 0, 0, 0),
(67, 2, 2, 3, 15, '216', '2017-07-21 11:21:59', '2017-07-21 11:21:59', 0, 0, 0, 0),
(68, 2, 2, 3, 15, '217', '2017-07-21 11:22:08', '2017-07-21 11:22:08', 0, 0, 0, 0),
(69, 2, 2, 3, 15, '218', '2017-07-21 11:22:12', '2017-07-21 11:22:12', 0, 0, 0, 0),
(70, 2, 2, 3, 15, '219', '2017-07-21 11:22:16', '2017-07-21 11:22:16', 0, 0, 0, 0),
(71, 2, 2, 3, 15, '220', '2017-07-21 11:22:19', '2017-07-21 11:22:19', 0, 0, 0, 0),
(72, 2, 2, 3, 16, '221', '2017-07-21 11:22:33', '2017-07-21 11:22:33', 0, 0, 0, 0),
(73, 2, 2, 3, 16, '222', '2017-07-21 11:22:36', '2017-07-21 11:22:36', 0, 0, 0, 0),
(74, 2, 2, 3, 16, '223', '2017-07-21 11:22:40', '2017-07-21 11:22:40', 0, 0, 0, 0),
(75, 2, 2, 3, 16, '224', '2017-07-21 11:22:47', '2017-07-21 11:22:47', 0, 0, 0, 0),
(76, 2, 2, 3, 16, '225', '2017-07-21 11:22:50', '2017-07-21 11:22:50', 0, 0, 0, 0),
(77, 2, 2, 3, 16, '226', '2017-07-21 11:22:53', '2017-07-21 11:22:53', 0, 0, 0, 0),
(78, 2, 2, 3, 16, '227', '2017-07-21 11:22:56', '2017-07-21 11:22:56', 0, 0, 0, 0),
(79, 2, 2, 3, 16, '228', '2017-07-21 11:22:59', '2017-07-21 11:22:59', 0, 0, 0, 0),
(80, 2, 2, 3, 16, '229', '2017-07-21 11:23:02', '2017-07-21 11:23:02', 0, 0, 0, 0),
(81, 2, 2, 3, 16, '230', '2017-07-21 11:23:07', '2017-07-21 11:23:07', 0, 0, 0, 0),
(82, 2, 2, 3, 16, '231', '2017-07-21 11:23:16', '2017-07-21 11:23:16', 0, 0, 0, 0),
(83, 2, 2, 3, 17, 'Day Care 1', '2017-07-21 11:24:32', '2017-07-21 11:24:32', 0, 0, 0, 0),
(84, 2, 2, 3, 17, 'Day Care 2', '2017-07-21 11:24:37', '2017-07-21 11:24:37', 0, 0, 0, 0),
(85, 2, 2, 3, 17, 'Day Care 3', '2017-07-21 11:24:42', '2017-07-21 11:24:42', 0, 0, 0, 0),
(86, 2, 2, 3, 17, 'Day Care 4', '2017-07-21 11:24:47', '2017-07-21 11:24:47', 0, 0, 0, 0),
(87, 2, 2, 3, 17, '232/1', '2017-07-21 11:25:01', '2017-07-21 11:25:01', 0, 0, 0, 0),
(88, 2, 2, 3, 17, '232/2', '2017-07-21 11:25:10', '2017-07-21 11:25:10', 0, 0, 0, 0),
(89, 2, 2, 3, 17, '232/3', '2017-07-21 11:25:15', '2017-07-21 11:25:15', 0, 0, 0, 0),
(90, 2, 2, 3, 17, '232/4', '2017-07-21 11:25:19', '2017-07-21 11:25:19', 0, 0, 0, 0),
(91, 2, 2, 3, 17, '233/1', '2017-07-21 11:25:24', '2017-07-21 11:25:24', 0, 0, 0, 0),
(92, 2, 2, 3, 17, '233/2', '2017-07-21 11:25:29', '2017-07-21 11:25:29', 0, 0, 0, 0),
(93, 2, 2, 3, 17, '233/4', '2017-07-21 11:25:34', '2017-07-21 11:25:34', 0, 0, 0, 0),
(94, 2, 2, 3, 17, '233/3', '2017-07-21 11:25:44', '2017-07-21 11:25:44', 0, 0, 0, 0),
(95, 2, 2, 3, 17, '234/1', '2017-07-21 11:26:10', '2017-07-21 11:26:10', 0, 0, 0, 0),
(97, 2, 2, 3, 17, '234/2', '2017-07-21 11:26:29', '2017-07-21 11:26:29', 0, 0, 0, 0),
(98, 2, 2, 3, 17, '235/1', '2017-07-21 11:26:44', '2017-07-21 11:26:44', 0, 0, 0, 0),
(99, 2, 2, 3, 17, '235/2', '2017-07-21 11:26:49', '2017-07-21 11:26:49', 0, 0, 0, 0),
(100, 2, 2, 3, 17, '236/1', '2017-07-21 11:26:54', '2017-07-21 11:26:54', 0, 0, 0, 0),
(101, 2, 2, 3, 17, '236/2', '2017-07-21 11:26:59', '2017-07-21 11:26:59', 0, 0, 0, 0),
(102, 2, 2, 3, 17, '237/1', '2017-07-21 11:27:05', '2017-07-21 11:27:05', 0, 0, 0, 0),
(103, 2, 2, 3, 17, '237/2', '2017-07-21 11:27:11', '2017-07-21 11:27:11', 0, 0, 0, 0),
(104, 2, 2, 3, 17, '238/1', '2017-07-21 11:27:16', '2017-07-21 11:27:16', 0, 0, 0, 0),
(105, 2, 2, 3, 17, '238/2', '2017-07-21 11:27:24', '2017-07-21 11:27:24', 0, 0, 0, 0),
(106, 3, 3, 4, 32, '10', '2017-07-26 11:12:02', '2017-07-26 11:12:02', 0, 0, 0, 0),
(107, 3, 3, 4, 31, '22', '2017-07-26 11:12:54', '2017-07-26 11:12:54', 0, 0, 0, 0),
(108, 3, 3, 4, 32, '11', '2017-07-26 11:23:13', '2017-07-26 11:23:13', 0, 0, 0, 0),
(109, 2, 2, 3, 9, 'Other', '2017-07-31 07:23:34', '2017-07-31 07:23:34', 0, 0, 0, 0),
(110, 2, 2, 3, 12, 'Other', '2017-07-31 07:23:44', '2017-07-31 07:23:44', 0, 0, 0, 0),
(111, 2, 2, 3, 13, 'Other', '2017-07-31 07:23:51', '2017-07-31 07:23:51', 0, 0, 0, 0),
(112, 2, 2, 3, 14, 'Other', '2017-07-31 07:23:59', '2017-07-31 07:23:59', 0, 0, 0, 0),
(113, 2, 2, 3, 15, 'Other', '2017-07-31 07:24:06', '2017-07-31 07:24:06', 0, 0, 0, 0),
(114, 2, 2, 3, 16, 'Other', '2017-07-31 07:24:13', '2017-07-31 07:24:13', 0, 0, 0, 0),
(115, 2, 2, 3, 17, 'Other', '2017-07-31 07:24:19', '2017-07-31 07:24:19', 0, 0, 0, 0),
(116, 3, 3, 4, 35, '200', '2017-08-01 10:51:49', '2017-08-01 10:51:49', 0, 0, 0, 0),
(117, 3, 3, 4, 35, '201', '2017-08-01 10:51:55', '2017-08-01 10:51:55', 0, 0, 0, 0),
(118, 3, 3, 4, 35, '202', '2017-08-01 10:52:00', '2017-08-01 10:52:00', 0, 0, 0, 0),
(119, 3, 3, 4, 35, '203', '2017-08-01 10:52:05', '2017-08-01 10:52:05', 0, 0, 0, 0),
(120, 3, 3, 4, 35, '204', '2017-08-01 10:52:13', '2017-08-01 10:52:13', 0, 0, 0, 0),
(121, 3, 3, 4, 35, '205', '2017-08-01 10:52:18', '2017-08-01 10:52:18', 0, 0, 0, 0),
(122, 3, 3, 4, 36, '206', '2017-08-01 10:52:38', '2017-08-01 10:52:38', 0, 0, 0, 0),
(123, 3, 3, 4, 36, '207', '2017-08-01 10:52:46', '2017-08-01 10:52:46', 0, 0, 0, 0),
(124, 3, 3, 4, 36, '208', '2017-08-01 10:52:52', '2017-08-01 10:52:52', 0, 0, 0, 0),
(125, 3, 3, 4, 36, '209', '2017-08-01 10:52:56', '2017-08-01 10:52:56', 0, 0, 0, 0),
(126, 3, 3, 4, 37, '300', '2017-08-01 10:54:26', '2017-08-01 10:54:26', 0, 0, 0, 0),
(127, 3, 3, 4, 37, '301', '2017-08-01 10:54:31', '2017-08-01 10:54:31', 0, 0, 0, 0),
(128, 3, 3, 4, 37, '302', '2017-08-01 10:54:36', '2017-08-01 10:54:36', 0, 0, 0, 0),
(129, 3, 3, 4, 37, '304', '2017-08-01 10:54:41', '2017-08-01 10:54:41', 0, 0, 0, 0),
(130, 3, 3, 4, 37, '305', '2017-08-01 10:54:47', '2017-08-01 10:54:47', 0, 0, 0, 0),
(131, 3, 3, 4, 38, '306', '2017-08-01 10:54:59', '2017-08-01 10:54:59', 0, 0, 0, 0),
(132, 3, 3, 4, 38, '307', '2017-08-01 10:55:03', '2017-08-01 10:55:03', 0, 0, 0, 0),
(133, 3, 3, 4, 38, '308', '2017-08-01 10:55:07', '2017-08-01 10:55:07', 0, 0, 0, 0),
(134, 3, 3, 4, 38, '309', '2017-08-01 10:55:12', '2017-08-01 10:55:12', 0, 0, 0, 0),
(135, 3, 3, 4, 38, '310', '2017-08-01 10:55:16', '2017-08-01 10:55:16', 0, 0, 0, 0),
(136, 3, 3, 4, 39, '311', '2017-08-01 10:55:35', '2017-08-01 10:55:35', 0, 0, 0, 0),
(137, 3, 3, 4, 39, '312', '2017-08-01 10:55:42', '2017-08-01 10:55:42', 0, 0, 0, 0),
(138, 3, 3, 4, 39, '313', '2017-08-01 10:55:46', '2017-08-01 10:55:46', 0, 0, 0, 0),
(139, 3, 3, 4, 39, '314', '2017-08-01 10:55:51', '2017-08-01 10:55:51', 0, 0, 0, 0),
(140, 3, 3, 4, 39, '315', '2017-08-01 10:55:55', '2017-08-01 10:55:55', 0, 0, 0, 0),
(141, 3, 3, 4, 39, '316', '2017-08-01 10:56:00', '2017-08-01 10:56:00', 0, 0, 0, 0),
(142, 3, 3, 4, 39, '317', '2017-08-01 10:56:05', '2017-08-01 10:56:05', 0, 0, 0, 0),
(143, 3, 3, 4, 39, '318', '2017-08-01 10:56:09', '2017-08-01 10:56:09', 0, 0, 0, 0),
(144, 3, 3, 4, 39, '319', '2017-08-01 10:56:14', '2017-08-01 10:56:14', 0, 0, 0, 0),
(145, 3, 3, 4, 39, '320', '2017-08-01 10:56:21', '2017-08-01 10:56:21', 0, 0, 0, 0),
(146, 3, 3, 4, 32, '1', '2017-08-03 05:16:01', '2017-08-03 05:16:01', 0, 0, 0, 0),
(147, 3, 3, 4, 32, '2', '2017-08-03 05:16:11', '2017-08-03 05:16:11', 0, 0, 0, 0),
(148, 3, 3, 4, 32, '3', '2017-08-03 05:16:20', '2017-08-03 05:16:20', 0, 0, 0, 0),
(149, 3, 3, 4, 32, '4', '2017-08-03 05:16:42', '2017-08-03 05:16:42', 0, 0, 0, 0),
(150, 3, 3, 4, 32, '5', '2017-08-03 05:16:52', '2017-08-03 05:16:52', 0, 0, 0, 0),
(151, 3, 3, 4, 32, '6', '2017-08-03 05:17:01', '2017-08-03 05:17:01', 0, 0, 0, 0),
(152, 3, 3, 4, 32, '7', '2017-08-03 05:17:07', '2017-08-03 05:17:07', 0, 0, 0, 0),
(153, 3, 3, 4, 32, '8', '2017-08-03 05:17:15', '2017-08-03 05:17:15', 0, 0, 0, 0),
(154, 3, 3, 4, 32, '9', '2017-08-03 05:17:23', '2017-08-03 05:17:23', 0, 0, 0, 0),
(155, 2, 2, 3, 9, '1A - Clean Utility', '2017-08-14 07:32:40', '2017-08-14 07:32:40', 0, 0, 0, 0),
(156, 2, 2, 3, 9, '1A - Dirty Utility', '2017-08-14 07:32:53', '2017-08-14 07:32:53', 0, 0, 0, 0),
(157, 2, 2, 3, 12, '1B - Clean Utility', '2017-08-14 07:33:19', '2017-08-14 07:33:19', 0, 0, 0, 0),
(158, 2, 2, 3, 12, '1B - Dirty Utility', '2017-08-14 07:33:31', '2017-08-14 07:33:31', 0, 0, 0, 0),
(159, 2, 2, 3, 13, '1C - Clean Utility', '2017-08-14 07:33:50', '2017-08-14 07:33:50', 0, 0, 0, 0),
(160, 2, 2, 3, 13, '1C - Dirty Utility', '2017-08-14 07:33:58', '2017-08-14 07:33:58', 0, 0, 0, 0),
(161, 2, 2, 3, 14, '2A - Clean Utility', '2017-08-14 07:34:14', '2017-08-14 07:34:14', 0, 0, 0, 0),
(162, 2, 2, 3, 14, '2A - Dirty Utility', '2017-08-14 07:34:21', '2017-08-14 07:34:21', 0, 0, 0, 0),
(163, 2, 2, 3, 15, '2B - Clean Utility', '2017-08-14 07:34:37', '2017-08-14 07:34:37', 0, 0, 0, 0),
(164, 2, 2, 3, 15, '2B - Dirty Utility', '2017-08-14 07:34:43', '2017-08-14 07:34:43', 0, 0, 0, 0),
(165, 2, 2, 3, 16, '2C - Clean Utility', '2017-08-14 07:34:55', '2017-08-14 07:34:55', 0, 0, 0, 0),
(166, 2, 2, 3, 16, '2C - Dirty Utility', '2017-08-14 07:35:16', '2017-08-14 07:35:16', 0, 0, 0, 0),
(167, 2, 2, 3, 17, '2D - Clean Utility', '2017-08-14 07:35:26', '2017-08-14 07:35:26', 0, 0, 0, 0),
(168, 2, 2, 3, 17, '2D - Dirty Utility', '2017-08-14 07:35:36', '2017-08-14 07:35:36', 0, 0, 0, 0),
(169, 2, 2, 3, 9, '107/1', '2017-10-14 05:38:22', '2017-10-14 05:38:22', 0, 0, 0, 0),
(170, 2, 2, 3, 9, '107/2', '2017-10-14 05:39:10', '2017-10-14 05:39:10', 0, 0, 0, 0),
(171, 4, 4, 5, 45, '851', '2017-10-17 11:59:50', '2017-10-17 11:59:50', 0, 0, 0, 0),
(172, 4, 4, 5, 45, '852A', '2017-10-17 12:00:05', '2017-10-17 12:00:05', 0, 0, 0, 0),
(173, 4, 4, 5, 45, '852B', '2017-10-17 12:00:11', '2017-10-17 12:00:11', 0, 0, 0, 0),
(174, 4, 4, 5, 45, '853', '2017-10-17 12:00:26', '2017-10-17 12:00:26', 0, 0, 0, 0),
(175, 4, 4, 5, 45, '854', '2017-10-17 12:00:31', '2017-10-17 12:00:31', 0, 0, 0, 0),
(176, 4, 4, 5, 45, '855', '2017-10-17 12:00:34', '2017-10-17 12:00:34', 0, 0, 0, 0),
(177, 4, 4, 5, 45, '856', '2017-10-17 12:00:38', '2017-10-17 12:00:38', 0, 0, 0, 0),
(178, 4, 4, 5, 45, '857', '2017-10-17 12:00:42', '2017-10-17 12:00:42', 0, 0, 0, 0),
(179, 4, 4, 5, 45, '858', '2017-10-17 12:00:47', '2017-10-17 12:00:47', 0, 0, 0, 0),
(180, 4, 4, 5, 45, '859', '2017-10-17 12:00:50', '2017-10-17 12:00:50', 0, 0, 0, 0),
(181, 4, 4, 5, 45, '860', '2017-10-17 12:00:57', '2017-10-17 12:00:57', 0, 0, 0, 0),
(182, 4, 4, 5, 47, '861', '2017-10-17 12:01:31', '2017-10-17 12:01:31', 0, 0, 0, 0),
(183, 4, 4, 5, 47, '862', '2017-10-17 12:01:37', '2017-10-17 12:01:37', 0, 0, 0, 0),
(184, 4, 4, 5, 47, '863', '2017-10-17 12:01:41', '2017-10-17 12:01:41', 0, 0, 0, 0),
(185, 4, 4, 5, 47, '864', '2017-10-17 12:01:45', '2017-10-17 12:01:45', 0, 0, 0, 0),
(186, 4, 4, 5, 47, '865', '2017-10-17 12:01:48', '2017-10-17 12:01:48', 0, 0, 0, 0),
(187, 4, 4, 5, 47, '866', '2017-10-17 12:01:51', '2017-10-17 12:01:51', 0, 0, 0, 0),
(188, 4, 4, 5, 47, '867', '2017-10-17 12:01:56', '2017-10-17 12:01:56', 0, 0, 0, 0),
(189, 4, 4, 5, 47, '868', '2017-10-17 12:01:59', '2017-10-17 12:01:59', 0, 0, 0, 0),
(190, 4, 4, 5, 47, '869', '2017-10-17 12:02:02', '2017-10-17 12:02:02', 0, 0, 0, 0),
(191, 4, 4, 5, 47, '870', '2017-10-17 12:02:06', '2017-10-17 12:02:06', 0, 0, 0, 0),
(192, 4, 4, 5, 47, '871A', '2017-10-17 12:02:15', '2017-10-17 12:02:15', 0, 0, 0, 0),
(193, 4, 4, 5, 47, '871B', '2017-10-17 12:02:19', '2017-10-17 12:02:19', 0, 0, 0, 0),
(194, 4, 4, 5, 47, '872', '2017-10-17 12:02:27', '2017-10-17 12:02:27', 0, 0, 0, 0),
(195, 4, 4, 5, 47, '873', '2017-10-17 12:02:30', '2017-10-17 12:02:30', 0, 0, 0, 0),
(196, 4, 4, 5, 47, '874', '2017-10-17 12:02:35', '2017-10-17 12:02:35', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_devices`
--

CREATE TABLE `rc_hospital_devices` (
  `hospital_device_id` double NOT NULL,
  `umbrella` double NOT NULL,
  `hospital` double NOT NULL,
  `hospital_location` double DEFAULT NULL,
  `hospital_area` double DEFAULT NULL,
  `device_number` varchar(20) NOT NULL COMMENT 'This will be intrinsically unique',
  `device_name` varchar(100) NOT NULL,
  `is_deactivated` tinyint(1) NOT NULL DEFAULT '0',
  `is_assigned` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `created_by` double NOT NULL,
  `modified_by` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='A list of all the devices provided to the - Hospital -\r\nWhen a device is first entered here it will be active. The device can be deactivated for repairs or if faulty. \r\nThe logic will check if there is a log of assigments to the device. If there is a log then the device is flagged as deactive else this record can be deleted.\r\nThe is_assigned flag will be manipulated by the code.  If the assigned_until date of assignments is entered then it is flagged to - 0 -. If there is only a from date then this value will be - 1 -\r\n';

--
-- Dumping data for table `rc_hospital_devices`
--

INSERT INTO `rc_hospital_devices` (`hospital_device_id`, `umbrella`, `hospital`, `hospital_location`, `hospital_area`, `device_number`, `device_name`, `is_deactivated`, `is_assigned`, `created_on`, `modified_on`, `created_by`, `modified_by`) VALUES
(1, 1, 1, NULL, NULL, '0000', 'Device 0', 1, 0, '2017-07-14 17:36:00', '2017-07-26 11:22:16', 0, 0),
(2, 1, 1, NULL, NULL, '0000017', 'Device 9', 0, 0, '2017-07-14 17:37:02', '2017-09-25 06:12:44', 0, 0),
(3, 1, 1, 1, 4, '000001', 'Device 1', 1, 0, '2017-07-14 17:37:30', '2017-07-26 07:27:35', 0, 0),
(4, 1, 1, 1, 2, '000005', 'Device 5', 1, 0, '2017-07-14 17:38:06', '2017-07-26 07:27:40', 0, 0),
(5, 1, 1, NULL, NULL, '0003', 'Device 3', 1, 0, '2017-07-14 17:38:33', '2017-07-26 11:22:14', 0, 0),
(6, 1, 1, 1, 5, '000002', 'Device 2', 1, 0, '2017-07-14 17:39:08', '2017-07-26 07:27:37', 0, 0),
(7, 1, 1, NULL, NULL, '0007', 'Device 7', 1, 0, '2017-07-14 17:49:48', '2017-07-26 11:22:12', 0, 0),
(9, 3, 3, 4, 32, '000000', 'Device 1', 0, 1, '2017-07-26 11:21:27', '2017-07-27 06:14:16', 0, 0),
(10, 3, 3, 4, 31, '000007', 'Device 3', 0, 1, '2017-07-26 11:22:15', '2017-07-26 11:22:15', 0, 0),
(11, 3, 3, 4, 32, '000003', 'Device2', 0, 1, '2017-07-26 11:24:01', '2017-07-26 11:24:01', 0, 0),
(12, 3, 3, 4, 35, '000009', '000009', 0, 1, '2017-09-25 06:13:30', '2017-09-25 06:13:30', 0, 0),
(13, 4, 4, 5, 45, '00000a', 'Device a', 1, 1, '2017-10-24 05:30:48', '2017-10-24 05:30:48', 0, 0),
(14, 4, 4, 5, 45, '00000c', 'Device c', 0, 1, '2017-10-24 05:31:39', '2017-10-24 05:31:39', 0, 0),
(15, 4, 4, 5, 45, '00000e', 'Device e', 0, 1, '2017-10-24 05:32:17', '2017-10-24 05:32:17', 0, 0),
(16, 4, 4, 5, 45, '00000g', 'Device g', 1, 1, '2017-10-24 05:32:53', '2017-10-24 05:32:53', 0, 0),
(17, 4, 4, 5, 45, '00000b', 'Device b', 0, 1, '2017-10-24 05:34:48', '2017-10-24 05:34:48', 0, 0),
(18, 3, 3, 4, 29, '00000d', 'Device d', 1, 1, '2017-10-24 05:39:27', '2017-10-24 05:39:27', 0, 0),
(19, 4, 4, 5, 45, '00000f', 'Device f', 1, 1, '2017-10-24 05:40:09', '2017-10-24 05:40:09', 0, 0),
(20, 4, 4, 5, 45, '00000h', 'Device h', 1, 1, '2017-10-24 05:40:31', '2017-10-24 05:40:31', 0, 0),
(21, 4, 4, 5, 45, '00000i', 'Device i', 0, 1, '2017-10-24 05:40:53', '2017-10-24 05:40:53', 0, 0),
(22, 4, 4, 5, 45, '00000j', 'Device j', 0, 1, '2017-10-24 05:41:22', '2017-10-24 05:41:22', 0, 0),
(23, 4, 4, 5, 45, '00000k', 'Device k', 0, 1, '2017-10-24 05:42:02', '2017-10-24 05:42:02', 0, 0),
(24, 4, 4, 5, 47, '00000l', 'Device l', 1, 1, '2017-10-24 05:45:18', '2017-10-24 05:45:18', 0, 0),
(25, 4, 4, 5, 47, '00000m', 'device m', 1, 1, '2017-10-24 05:45:39', '2017-10-24 05:45:39', 0, 0),
(26, 4, 4, 5, 47, '00000n', 'Device n', 0, 1, '2017-10-24 05:46:10', '2017-10-24 05:46:10', 0, 0),
(27, 4, 4, 5, 47, '00000o', 'Device o', 0, 1, '2017-10-24 05:46:31', '2017-10-24 05:46:31', 0, 0),
(28, 4, 4, 5, 47, '00000p', 'Device p', 1, 1, '2017-10-24 05:46:50', '2017-10-24 05:46:50', 0, 0),
(29, 4, 4, 5, 47, '00000q', 'Device q', 0, 1, '2017-10-24 05:47:16', '2017-10-24 05:47:16', 0, 0),
(30, 4, 4, 5, 47, '00000r', 'Device r', 0, 1, '2017-10-24 05:47:41', '2017-10-24 05:47:41', 0, 0),
(31, 4, 4, 5, 47, '00000s', 'Device s', 0, 1, '2017-10-24 05:48:02', '2017-10-24 05:48:02', 0, 0),
(32, 4, 4, 5, 47, '00000t', 'Device t', 0, 1, '2017-10-24 05:48:41', '2017-10-24 05:48:41', 0, 0),
(33, 4, 4, 5, 47, '00000u', 'Device u', 0, 1, '2017-10-24 05:49:11', '2017-10-24 05:49:11', 0, 0),
(34, 4, 4, 5, 47, '00000v', 'Device v', 0, 1, '2017-10-24 05:49:41', '2017-10-24 05:49:41', 0, 0),
(35, 4, 4, 5, 47, '00000w', 'Device w', 0, 1, '2017-10-24 05:50:08', '2017-10-24 05:50:08', 0, 0),
(36, 4, 4, 5, 47, '00000x', 'Device x', 0, 1, '2017-10-24 05:50:37', '2017-10-24 05:50:37', 0, 0),
(37, 4, 4, 5, 47, '00000y', 'Device y', 0, 1, '2017-10-24 05:51:01', '2017-10-24 05:51:01', 0, 0),
(38, 4, 4, 5, 47, '00000z', 'Device z', 1, 0, '2017-10-24 05:51:34', '2017-10-24 05:51:34', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_device_bed_assign`
--

CREATE TABLE `rc_hospital_device_bed_assign` (
  `device_bed_assign_id` double NOT NULL,
  `umbrella` double NOT NULL,
  `hospital` double NOT NULL,
  `hospital_location` double NOT NULL,
  `hospital_area` double NOT NULL,
  `hospital_bed` double NOT NULL,
  `hospital_device` double NOT NULL,
  `assigned_from` datetime NOT NULL,
  `assigned_until` datetime DEFAULT NULL COMMENT 'If currently active then this value will be null.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Assigns a device to a bed.\r\nThe from and to dates will help to maintain a log of assignments. Devices may be temprarily unavailable or may be deactivated.\r\n';

--
-- Dumping data for table `rc_hospital_device_bed_assign`
--

INSERT INTO `rc_hospital_device_bed_assign` (`device_bed_assign_id`, `umbrella`, `hospital`, `hospital_location`, `hospital_area`, `hospital_bed`, `hospital_device`, `assigned_from`, `assigned_until`) VALUES
(1, 1, 1, 1, 3, 3, 1, '2017-07-14 17:36:00', '2017-07-26 07:27:33'),
(2, 1, 1, 1, 3, 4, 2, '2017-07-14 17:37:02', '2017-07-26 07:27:45'),
(3, 1, 1, 1, 4, 5, 3, '2017-07-14 17:37:30', '2017-07-26 07:27:35'),
(4, 1, 1, 1, 2, 1, 4, '2017-07-14 17:38:06', '2017-07-26 07:27:40'),
(5, 1, 1, 1, 2, 2, 5, '2017-07-14 17:38:33', '2017-07-26 07:27:38'),
(6, 1, 1, 1, 5, 6, 6, '2017-07-14 17:39:08', '2017-07-26 07:27:37'),
(7, 1, 1, 1, 5, 7, 7, '2017-07-14 17:39:43', '2017-07-26 07:27:42'),
(8, 2, 2, 3, 9, 8, 8, '2017-07-21 13:08:24', '2017-07-21 13:09:01'),
(9, 3, 3, 4, 32, 106, 9, '2017-07-26 11:21:27', NULL),
(10, 3, 3, 4, 31, 107, 10, '2017-07-26 11:22:15', NULL),
(11, 3, 3, 4, 32, 108, 11, '2017-07-26 11:24:01', NULL),
(12, 3, 3, 4, 32, 106, 9, '2017-07-27 06:15:00', NULL),
(13, 3, 3, 4, 35, 117, 12, '2017-09-25 06:13:30', NULL),
(14, 4, 4, 5, 45, 171, 13, '2017-10-24 05:30:48', '2017-11-18 03:27:19'),
(15, 4, 4, 5, 45, 173, 14, '2017-10-24 05:31:39', '2017-11-18 04:37:29'),
(16, 4, 4, 5, 45, 175, 15, '2017-10-24 05:32:17', NULL),
(17, 4, 4, 5, 45, 177, 16, '2017-10-24 05:32:53', NULL),
(18, 4, 4, 5, 45, 172, 17, '2017-10-24 05:34:48', '2017-11-18 02:56:37'),
(19, 3, 3, 4, 32, 106, 18, '2017-10-24 05:39:27', NULL),
(20, 4, 4, 5, 45, 176, 19, '2017-10-24 05:40:09', NULL),
(21, 4, 4, 5, 45, 178, 20, '2017-10-24 05:40:31', NULL),
(22, 4, 4, 5, 45, 179, 21, '2017-10-24 05:40:53', NULL),
(23, 4, 4, 5, 45, 180, 22, '2017-10-24 05:41:22', NULL),
(24, 4, 4, 5, 45, 181, 23, '2017-10-24 05:42:02', NULL),
(25, 4, 4, 5, 47, 182, 24, '2017-10-24 05:45:18', NULL),
(26, 4, 4, 5, 47, 183, 25, '2017-10-24 05:45:39', NULL),
(27, 4, 4, 5, 47, 184, 26, '2017-10-24 05:46:10', NULL),
(28, 4, 4, 5, 47, 185, 27, '2017-10-24 05:46:31', NULL),
(29, 4, 4, 5, 47, 186, 28, '2017-10-24 05:46:50', NULL),
(30, 4, 4, 5, 47, 187, 29, '2017-10-24 05:47:16', NULL),
(31, 4, 4, 5, 47, 188, 30, '2017-10-24 05:47:41', NULL),
(32, 4, 4, 5, 47, 189, 31, '2017-10-24 05:48:02', NULL),
(33, 4, 4, 5, 47, 190, 32, '2017-10-24 05:48:41', NULL),
(34, 4, 4, 5, 47, 191, 33, '2017-10-24 05:49:11', NULL),
(35, 4, 4, 5, 47, 192, 34, '2017-10-24 05:49:41', NULL),
(36, 4, 4, 5, 47, 193, 35, '2017-10-24 05:50:08', NULL),
(37, 4, 4, 5, 47, 194, 36, '2017-10-24 05:50:37', NULL),
(38, 4, 4, 5, 47, 195, 37, '2017-10-24 05:51:01', NULL),
(39, 4, 4, 5, 45, 171, 13, '2017-11-18 03:37:07', '2017-11-18 04:37:12'),
(40, 4, 4, 5, 45, 171, 13, '2017-11-18 04:39:37', '2017-11-18 04:45:36'),
(41, 4, 4, 5, 45, 171, 14, '2017-11-18 04:45:36', '2017-11-18 04:45:45'),
(42, 4, 4, 5, 45, 171, 38, '2017-11-18 04:45:45', '2017-11-18 06:09:49'),
(43, 4, 4, 5, 47, 196, 13, '2017-11-18 04:45:58', NULL),
(44, 4, 4, 5, 45, 171, 14, '2017-11-18 06:09:49', '2017-11-20 03:26:15'),
(45, 4, 4, 5, 45, 172, 38, '2017-11-18 06:09:59', '2017-11-20 03:17:38'),
(46, 4, 4, 5, 45, 171, 14, '2017-11-20 03:26:35', '2017-11-20 03:37:49'),
(47, 4, 4, 5, 45, 172, 38, '2017-11-20 03:27:26', '2017-11-20 06:44:09'),
(48, 4, 4, 5, 45, 171, 14, '2017-11-20 06:44:03', '2017-11-20 06:44:12'),
(49, 4, 4, 5, 45, 171, 14, '2017-11-26 05:27:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_location_areas`
--

CREATE TABLE `rc_hospital_location_areas` (
  `hospital_area_id` double NOT NULL,
  `umbrella` double NOT NULL,
  `hospital` double NOT NULL,
  `hospital_location` double NOT NULL,
  `lft_ref` double NOT NULL,
  `rgt_ref` double NOT NULL,
  `level_number` tinyint(2) NOT NULL,
  `hospital_area_class` double NOT NULL,
  `area_name` varchar(200) NOT NULL,
  `area_description` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `created_by` double NOT NULL,
  `modified_by` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table will granularise the hospital location into an atom level definition. \r\nThe lft_ref and rgt_ref fields will determine the hierrachy of the atom.';

--
-- Dumping data for table `rc_hospital_location_areas`
--

INSERT INTO `rc_hospital_location_areas` (`hospital_area_id`, `umbrella`, `hospital`, `hospital_location`, `lft_ref`, `rgt_ref`, `level_number`, `hospital_area_class`, `area_name`, `area_description`, `created_on`, `modified_on`, `created_by`, `modified_by`) VALUES
(1, 1, 1, 1, 1, 20, 0, 1, '4th Floor', '4th Floor', '2017-07-14 14:20:12', '2017-07-14 14:20:12', 0, 0),
(2, 1, 1, 1, 2, 3, 1, 2, '4th Floor - General Ward', 'General Ward', '2017-07-14 15:06:14', '2017-07-14 17:41:07', 0, 0),
(3, 1, 1, 1, 4, 5, 1, 3, '4th Floor - 401', 'Double-Bed Room', '2017-07-14 15:07:12', '2017-07-14 17:40:28', 0, 0),
(4, 1, 1, 1, 6, 7, 1, 4, '4th Floor - 402', 'Single-Bed Room (AC)', '2017-07-14 15:08:00', '2017-07-14 17:40:43', 0, 0),
(5, 1, 1, 1, 8, 9, 1, 3, '4th Floor - 410', 'Double-Bed Room', '2017-07-14 17:30:22', '2017-07-14 17:40:55', 0, 0),
(6, 2, 2, 3, 1, 8, 0, 1, 'First Floor', 'First Floor', '2017-07-20 16:43:06', '2017-07-20 16:43:06', 0, 0),
(7, 2, 2, 3, 9, 18, 0, 1, 'Second Floor', 'Second Floor', '2017-07-20 16:43:19', '2017-07-20 16:43:19', 0, 0),
(9, 2, 2, 3, 2, 3, 1, 1, '1 A Wing', '1 A Wing', '2017-07-20 16:43:59', '2017-07-20 16:43:59', 0, 0),
(12, 2, 2, 3, 4, 5, 1, 1, '1 B Wing', '1 B Wing', '2017-07-20 16:45:10', '2017-07-20 16:45:10', 0, 0),
(13, 2, 2, 3, 6, 7, 1, 1, '1 C Wing', '1 C Wing', '2017-07-20 16:45:25', '2017-07-20 16:45:25', 0, 0),
(14, 2, 2, 3, 10, 11, 1, 1, '2 A Wing', '2 A Wing', '2017-07-21 10:36:25', '2017-07-21 10:36:25', 0, 0),
(15, 2, 2, 3, 12, 13, 1, 1, '2 B Wing', '2 B Wing', '2017-07-21 10:36:45', '2017-07-21 10:36:45', 0, 0),
(16, 2, 2, 3, 14, 15, 1, 1, '2 C Wing', '2 C Wing', '2017-07-21 10:37:02', '2017-07-21 10:37:02', 0, 0),
(17, 2, 2, 3, 16, 17, 1, 1, '2 D Wing', '2 D Wing', '2017-07-21 10:37:24', '2017-07-21 10:37:24', 0, 0),
(21, 1, 1, 1, 21, 22, 0, 3, '5th Floor', '5th Floor', '2017-07-22 17:03:50', '2017-07-22 17:03:50', 0, 0),
(22, 1, 1, 1, 10, 17, 1, 3, 'one more room', 'one more room', '2017-07-22 17:04:18', '2017-07-22 17:04:18', 0, 0),
(23, 1, 1, 1, 23, 24, 0, 3, 'sub floor', 'sub floor', '2017-07-22 17:14:51', '2017-07-22 17:14:51', 0, 0),
(24, 1, 1, 1, 11, 12, 2, 5, 'dasfds', 'dfadsfas', '2017-07-22 17:15:26', '2017-07-22 17:15:26', 0, 0),
(25, 1, 1, 1, 25, 26, 0, 9, 'daf', 'dafd', '2017-07-22 17:52:11', '2017-07-22 17:52:11', 0, 0),
(26, 1, 1, 1, 13, 16, 2, 5, 'fadfa', 'dfasdf', '2017-07-22 17:52:31', '2017-07-22 17:52:31', 0, 0),
(27, 1, 1, 1, 14, 15, 3, 2, 'dfasd', 'dfadsf', '2017-07-22 17:52:40', '2017-07-22 17:52:40', 0, 0),
(28, 1, 1, 1, 18, 19, 1, 3, 'erqwerq', 'rqewrqw', '2017-07-22 17:54:03', '2017-07-22 17:54:03', 0, 0),
(29, 3, 3, 4, 1, 4, 0, 10, 'Ground floor', 'kids', '2017-07-26 11:08:36', '2017-07-26 16:38:36', 0, 0),
(30, 3, 3, 4, 5, 8, 0, 3, 'first floor', 'old age', '2017-07-26 11:08:59', '2017-07-26 16:38:59', 0, 0),
(31, 3, 3, 4, 6, 7, 1, 8, '1A', 'preg', '2017-07-26 11:10:49', '2017-07-26 16:40:49', 0, 0),
(32, 3, 3, 4, 2, 3, 1, 10, 'G1', 'teeth', '2017-07-26 11:11:28', '2017-07-26 16:41:28', 0, 0),
(33, 3, 3, 4, 9, 14, 0, 10, 'Second Floor', 'Second Floor', '2017-08-01 09:46:43', '2017-08-01 15:16:43', 0, 0),
(34, 3, 3, 4, 15, 26, 0, 6, 'Third Floor', 'Third Floor', '2017-08-01 09:47:02', '2017-08-01 15:17:02', 0, 0),
(35, 3, 3, 4, 10, 11, 1, 2, '2 A Wing', '2 A Wing', '2017-08-01 09:47:31', '2017-08-01 15:17:31', 0, 0),
(36, 3, 3, 4, 12, 13, 1, 6, '2 B Wing', '2 B Wing', '2017-08-01 09:47:58', '2017-08-01 15:17:58', 0, 0),
(37, 3, 3, 4, 16, 17, 1, 3, '3A Wing', '3A Wing', '2017-08-01 10:53:24', '2017-08-01 16:23:24', 0, 0),
(38, 3, 3, 4, 18, 21, 1, 6, '3B Wing', '3B Wing', '2017-08-01 10:53:38', '2017-08-01 16:23:38', 0, 0),
(39, 3, 3, 4, 22, 25, 1, 11, '3C Wing', '3C Wing', '2017-08-01 10:54:03', '2017-08-01 16:24:03', 0, 0),
(41, 3, 3, 4, 23, 24, 2, 5, 'dad', 'daad', '2017-08-03 12:05:00', '2017-08-03 17:35:00', 0, 0),
(43, 3, 3, 4, 19, 20, 2, 5, 'das', 'dfds', '2017-08-04 05:27:09', '2017-08-04 10:57:09', 0, 0),
(44, 4, 4, 5, 1, 6, 0, 3, '8th Floor', '8th Floor', '2017-10-17 11:58:05', '2017-10-17 17:28:04', 0, 0),
(45, 4, 4, 5, 2, 3, 1, 3, 'C Wing', 'C Wing', '2017-10-17 11:58:23', '2017-10-17 17:28:23', 0, 0),
(47, 4, 4, 5, 4, 5, 1, 3, 'D Wing', 'D Wing', '2017-10-17 11:58:51', '2017-10-17 17:28:51', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_location_area_classes`
--

CREATE TABLE `rc_hospital_location_area_classes` (
  `area_class_id` double NOT NULL,
  `hospital` double NOT NULL,
  `area_class_name` varchar(200) NOT NULL,
  `area_class_description` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `created_by` double NOT NULL,
  `modified_by` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='The different classes the area might refer to.\r\nDelux, Super delux, etc.';

--
-- Dumping data for table `rc_hospital_location_area_classes`
--

INSERT INTO `rc_hospital_location_area_classes` (`area_class_id`, `hospital`, `area_class_name`, `area_class_description`, `created_on`, `modified_on`, `created_by`, `modified_by`) VALUES
(1, 1, 'NA', 'No Class', '2017-07-14 13:27:54', '2017-07-14 14:00:48', 0, 0),
(2, 1, 'Economy', 'Economy', '2017-07-14 13:28:31', '2017-07-14 13:28:31', 0, 0),
(3, 1, 'Deluxe', 'Deluxe', '2017-07-14 13:28:47', '2017-07-14 13:28:47', 0, 0),
(4, 1, 'Super Deluxe', 'Super Deluxe', '2017-07-14 13:28:57', '2017-07-14 13:32:43', 0, 0),
(5, 2, 'Executive Suite', 'Executive Suite', '2017-07-21 10:59:03', '2017-07-21 10:59:03', 0, 0),
(6, 2, 'Double Bed', 'Double Bed', '2017-07-21 10:59:13', '2017-07-21 10:59:13', 0, 0),
(7, 2, 'Single Bed', 'Single Bed', '2017-07-21 10:59:27', '2017-07-21 10:59:27', 0, 0),
(8, 2, 'Superior Single', 'Superior Single', '2017-07-21 10:59:41', '2017-07-21 10:59:41', 0, 0),
(9, 2, 'Isolation Room', 'Isolation  Room', '2017-07-21 10:59:56', '2017-07-21 10:59:56', 0, 0),
(10, 2, 'Day Care', 'Day Care', '2017-07-21 11:00:03', '2017-07-21 11:00:03', 0, 0),
(11, 2, 'Four Bed', 'Four Bed', '2017-07-21 11:00:24', '2017-07-21 11:00:24', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_patient`
--

CREATE TABLE `rc_hospital_patient` (
  `hospital_patient_id` double NOT NULL,
  `hospital_location` double NOT NULL,
  `patient_number` varchar(12) NOT NULL,
  `patient_name` varchar(100) NOT NULL,
  `is_deactivated` tinyint(4) NOT NULL,
  `is_assigned` tinyint(4) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` double NOT NULL,
  `modified_by` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rc_hospital_patient`
--

INSERT INTO `rc_hospital_patient` (`hospital_patient_id`, `hospital_location`, `patient_number`, `patient_name`, `is_deactivated`, `is_assigned`, `created_on`, `modified_on`, `created_by`, `modified_by`) VALUES
(1, 1, '9008755216', 'Daniel', 0, 0, '2017-11-18 11:07:05', NULL, 1, NULL),
(2, 1, '9830988889', 'Darshan', 1, 0, '2017-11-20 06:20:21', NULL, 0, NULL),
(3, 1, '9830988888', 'Dev', 1, 1, '2017-11-21 02:36:00', NULL, 0, NULL),
(4, 1, '6153548135', 'sac', 1, 0, '2017-11-22 04:45:18', NULL, 0, NULL),
(5, 1, '24324', 'test', 1, 0, '2017-11-22 04:56:11', NULL, 0, NULL),
(6, 1, '12344', 'test2', 1, 0, '2017-11-22 04:56:26', NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rc_hospital_patient_bed_assign`
--

CREATE TABLE `rc_hospital_patient_bed_assign` (
  `patient_bed_assign_id` double NOT NULL,
  `hospital_bed` double NOT NULL,
  `hospital_patient` double NOT NULL,
  `assigned_from` datetime NOT NULL,
  `assigned_until` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table which stores all the patient bed assignments';

--
-- Dumping data for table `rc_hospital_patient_bed_assign`
--

INSERT INTO `rc_hospital_patient_bed_assign` (`patient_bed_assign_id`, `hospital_bed`, `hospital_patient`, `assigned_from`, `assigned_until`) VALUES
(1, 171, 2, '2017-11-20 06:20:21', '2017-11-20 06:12:47'),
(2, 172, 3, '2017-11-20 06:21:46', '2017-11-20 05:26:14'),
(3, 173, 2, '2017-11-21 02:36:00', '2017-11-22 04:53:34'),
(4, 174, 3, '2017-11-21 06:12:47', '2017-11-22 04:51:47'),
(5, 175, 4, '2017-11-22 04:45:18', '2017-11-22 04:51:30'),
(6, 171, 3, '2017-11-22 04:51:47', '2017-11-22 04:54:33'),
(7, 172, 2, '2017-11-22 04:53:34', '2017-11-22 06:19:30'),
(8, 171, 6, '2017-11-22 04:56:26', '2017-11-22 04:58:05'),
(9, 171, 2, '2017-11-22 06:20:35', '2017-11-22 06:51:21'),
(10, 172, 3, '2017-11-22 06:21:24', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rc_hospital_beds`
--
ALTER TABLE `rc_hospital_beds`
  ADD PRIMARY KEY (`hospital_bed_id`),
  ADD UNIQUE KEY `rc_hospital_beds_Nq_area_bed_number` (`hospital_location_area`,`bed_number`);

--
-- Indexes for table `rc_hospital_devices`
--
ALTER TABLE `rc_hospital_devices`
  ADD PRIMARY KEY (`hospital_device_id`),
  ADD UNIQUE KEY `rc_hospital_devices_Nq_device_number` (`device_number`),
  ADD KEY `rc_hospital_devices_Ndx_umbrella_hospital` (`umbrella`,`hospital`),
  ADD KEY `rc_hospital_devices_Nq_device_name` (`hospital`,`device_name`);

--
-- Indexes for table `rc_hospital_device_bed_assign`
--
ALTER TABLE `rc_hospital_device_bed_assign`
  ADD PRIMARY KEY (`device_bed_assign_id`),
  ADD UNIQUE KEY `rc_hospital_device_bed_assign_Nq_assignment` (`hospital_device`,`hospital_bed`,`assigned_from`,`assigned_until`);

--
-- Indexes for table `rc_hospital_location_areas`
--
ALTER TABLE `rc_hospital_location_areas`
  ADD PRIMARY KEY (`hospital_area_id`),
  ADD KEY `rc_hosptial_areas_Ndx_lft_ref_rgt_ref` (`lft_ref`,`rgt_ref`),
  ADD KEY `rc_hosptial_areas_Ndx_hospital_area_class` (`hospital_area_class`);

--
-- Indexes for table `rc_hospital_location_area_classes`
--
ALTER TABLE `rc_hospital_location_area_classes`
  ADD PRIMARY KEY (`area_class_id`),
  ADD KEY `rc_hospital_area_classes_Nq_area_class_name` (`area_class_name`);

--
-- Indexes for table `rc_hospital_patient`
--
ALTER TABLE `rc_hospital_patient`
  ADD PRIMARY KEY (`hospital_patient_id`);

--
-- Indexes for table `rc_hospital_patient_bed_assign`
--
ALTER TABLE `rc_hospital_patient_bed_assign`
  ADD PRIMARY KEY (`patient_bed_assign_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
