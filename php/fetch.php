 <?php

	require_once 'lib/_config.php'; 
	date_default_timezone_set('Asia/kolkata');
	$date =  date("Y-m-d h:i:s"); 	
		
	$password = $_GET['random'];
	$iterations = 1000;

	$salt = mcrypt_create_iv(24, MCRYPT_DEV_URANDOM);
	$salt = base64_encode($salt);
	$salt = str_replace('+', '.', $salt);

	$hash = hash_pbkdf2("sha256", $password, $salt, $iterations, 24);

	$patient_salt =$salt;
	$patient_hash =$hash ;


 //------------------------------------------------------------------------- LogIn Credentials  ---------------------------------------------------->

		 $sql = $link->query("SELECT * FROM `rc_hospital_staff` WHERE `rc_username` = '".$_GET['username']."' ");
		 $rows = $sql->fetch();
				
					
                        $umbrella = $rows['umbrella'];
                        $hospital = $rows['hospital'];
                        $staff_id = $rows['hospital_staff_id'];
						
		$sql = $link->query("SELECT * FROM `rc_hospital_location_areas` WHERE `umbrella`='".$umbrella."' AND `hospital`='".$hospital."'");
		$rows = $sql->fetch();
		$hospital_location = $rows['hospital_location'];

 //------------------------------------------------------------------------- AREAS  ---------------------------------------------------->
			$cont = 1;
		 $sql = $link->query("SELECT * FROM `rc_hospital_location_areas` WHERE `umbrella` = '".$umbrella."' AND `hospital` = '".$hospital."' ORDER BY `hospital_area_id` ASC");
		 while($rows = $sql->fetch())
				{
					
                        $areas[] = [
							"area_id"		     	  => $rows['hospital_area_id'],							
							"area"		     	  => $rows['area_name'],							
							];
							$cont++;
						}
					//	echo"<PRE>";
												
 //------------------------------------------------------------------------- BED-PATIENT ASSIGNMENT  ---------------------------------------------------->
						
		if($_GET["patient_details1"]=="show")
		{
			$sql = $link->query("Select 
								rhb.hospital_bed_id,
								rhb.bed_number
								from  rc_hospital_patient_bed_assign rhpb
								LEFT JOIN rc_hospital_patient rhp ON rhp.hospital_patient_id = rhpb.hospital_patient
								LEFT JOIN rc_hospital_beds rhb ON rhb.hospital_bed_id = rhpb.hospital_bed
								WHERE rhpb.assigned_from IS NOT NULL AND rhpb.assigned_until IS NULL order by rhp.hospital_patient_id ASC");
					 while($rows = $sql->fetch())
						{
					
                        $patient_details[] = [
							"hospital_bed_id"    	  => $rows['hospital_bed_id'],
							"bed_number"    	  => $rows['bed_number']
							];
							
						}
				echo json_encode($patient_details, JSON_PRETTY_PRINT);
		}
			
		if($_GET["patient_bed_details"]=="show")
		{
			$sql = $link->query("Select 
								rhb.hospital_bed_id,
								rhb.bed_number
								from  rc_hospital_patient_bed_assign rhpb
								LEFT JOIN rc_hospital_patient rhp ON rhp.hospital_patient_id = rhpb.hospital_patient
								LEFT JOIN rc_hospital_beds rhb ON rhb.hospital_bed_id = rhpb.hospital_bed
								WHERE rhpb.assigned_from IS NOT NULL AND rhpb.assigned_until IS NULL order by rhp.hospital_patient_id ASC");
					 while($rows = $sql->fetch())
						{
					
                        $patient_bed_details[] = [
							"hospital_bed_id"    	  => $rows['hospital_bed_id'],
							"bed_number"    	  => $rows['bed_number']
							];
							
						}
						
				echo json_encode($patient_bed_details, JSON_PRETTY_PRINT);
			
		}
			
		if($_GET["patient"]=="fetch_patient_number")
		{
			$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` = '".$_GET["bed_id"]."' AND `assigned_until` IS NULL ORDER BY `patient_bed_assign_id` DESC LIMIT 0,1");
			 $rows = $sql->fetch();

			$sql = $link->query("SELECT * FROM `rc_hospital_patient` WHERE `hospital_patient_id` = '".$rows['hospital_patient']."'");
			 $rows = $sql->fetch();

			echo $rows['patient_number'];
										
		}
			
		if($_GET["patient"]=="fetch_hospital_bed")
		{
			$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` = '".$_GET["bed_id"]."' AND `assigned_until` IS NULL ORDER BY `patient_bed_assign_id` DESC LIMIT 0,1");
			 $rows = $sql->fetch();

			$sql = $link->query("SELECT * FROM `rc_hospital_patient` WHERE `hospital_patient_id` = '".$rows['hospital_patient']."'");
			 $rows = $sql->fetch();

			echo $rows['patient_name'];
									
		}
			
		if($_GET["patient"]=="fetch_patient_number1")
		{

			$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` = '".$_GET["bed_id"]."' AND `assigned_until` IS NULL ORDER BY `patient_bed_assign_id` DESC LIMIT 0,1");
			 $rows = $sql->fetch();

			$sql = $link->query("SELECT * FROM `rc_hospital_patient` WHERE `hospital_patient_id` = '".$rows['hospital_patient']."'");
			 $rows = $sql->fetch();

			echo $rows['patient_number'];
										
		}
		
		if($_GET["patient"]=="fetch_patient_id1")
		{

			$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` = '".$_GET["bed_id"]."' AND `assigned_until` IS NULL ORDER BY `patient_bed_assign_id` DESC LIMIT 0,1");
			 $rows = $sql->fetch();

			$sql = $link->query("SELECT * FROM `rc_hospital_patient` WHERE `hospital_patient_id` = '".$rows['hospital_patient']."'");
			 $rows = $sql->fetch();

			echo $rows['hospital_patient_id'];
										
		}
			
		if($_GET["patient"]=="fetch_hospital_bed1")
		{
			$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` = '".$_GET["bed_id"]."' AND `assigned_until` IS NULL ORDER BY `patient_bed_assign_id` DESC LIMIT 0,1");
			 $rows = $sql->fetch();

			$sql = $link->query("SELECT * FROM `rc_hospital_patient` WHERE `hospital_patient_id` = '".$rows['hospital_patient']."'");
			 $rows = $sql->fetch();

			echo $rows['patient_name'];
									
		}
			
			
			
		if($_GET["patient_bed"]=="show")
		{
		 $sql = $link->query("SELECT * FROM `rc_hospital_beds` WHERE `umbrella` = '".$umbrella."' AND `hospital` = '".$hospital."' AND `hospital_location` = '".$hospital_location."' ORDER BY `hospital_bed_id` ASC");
		 while($rows = $sql->fetch())
				{
					
                        $beds[] = [
							"hospital_bed_id"    	  => $rows['hospital_bed_id'],
							"bed_number"     => $rows['bed_number']
							];
							
						}
		 $sql = $link->query("SELECT rhb.hospital_bed_id hospital_bed_id,
							rhb.bed_number bed_number
							
							FROM rc_hospital_patient_bed_assign  rhpb
							LEFT JOIN rc_hospital_beds rhb ON rhb.hospital_bed_id = rhpb.hospital_bed AND rhpb.assigned_until IS NULL
							WHERE   rhb.hospital='".$hospital."' AND rhb.umbrella='".$umbrella."' AND rhb.hospital_location='".$hospital_location."' ORDER BY rhb.hospital_bed_id	 ");
		 while($rows = $sql->fetch())
				{
					
                        $beds_assigned[] = [
							"hospital_bed_id"    	  => $rows['hospital_bed_id'],
							"bed_number"     => $rows['bed_number']
							];
							
						}
						for($i=0;$i<count($beds); $i++)
							{
							for($j=0;$j<count($beds_assigned); $j++)
								{
									if($beds[$i] == $beds_assigned[$j])
									{
										unset($beds[$i]);
									}
									
								}
							}
							
					$beds=	array_values($beds);
						
						
				echo json_encode($beds, JSON_PRETTY_PRINT);
		}
		
		
		
		if($_GET["patient_bed"]=="check_in" && $_GET['patient_name'] != NULL && $_GET['patient_number'] != NULL)
		{
						$patient_name = $_GET['patient_name'];
						$patient_number = $_GET['patient_number'];
						$hospital_bed = $_GET['bed_id'];
						if($hospital_bed!=NULL)
						{
							$is_assigned = 1;
						}
						else
						{
							$is_assigned = 0;
						}
		
		$sql = $link->query("SELECT * FROM `rc_hospital_patient` ");
		$num = $sql->rowCount();
		$num++;
		
		$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` ='".$hospital_bed."' AND `assigned_until` IS NULL  ");
		$count = $sql->rowCount();
		
		$sql = $link->query("SELECT * FROM `rc_hospital_patient` WHERE `patient_number` ='".$patient_number."'  ");
		$count1 = $sql->rowCount();
		
		if($count1 <= 0 && $count <= 0)				 
		{
		 $sql1 = $link->query("INSERT INTO `rc_hospital_patient`(`hospital_patient_id`, `umbrella`, `hospital`, `hospital_location`, `patient_number`, `patient_salt`, `patient_hash`, `patient_name`, `is_deactivated`, `is_assigned`, `created_on`, `modified_on`, `created_by`, `modified_by`) VALUES
			('".$num."','".$umbrella."','".$hospital."','".$hospital_location."','".htmlentities(trim(addslashes(strip_tags($patient_number))))."',
			'".htmlentities(trim(addslashes(strip_tags($patient_salt))))."','".htmlentities(trim(addslashes(strip_tags($patient_hash))))."',
			'".htmlentities(trim(addslashes(strip_tags($patient_name))))."','0',
			'".htmlentities(trim(addslashes(strip_tags($is_assigned))))."','".htmlentities(trim(addslashes(strip_tags($date))))."',NULL,'".$staff_id."',NULL)");
		}
		if($sql1)
		{
			echo "Patient Check-In Successful";
		}
		else
		{
			echo "Patient Number Already Exists";
		}
		if($hospital_bed != NULL && $count1 <= 0 && $count <= 0)
		{
		$hospital_patient_id = $num;;
		
		$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` ");
		$num1 = $sql->rowCount();
		$num1++;
						 
		 $sql = $link->query("INSERT INTO `rc_hospital_patient_bed_assign`(`patient_bed_assign_id`, `hospital_bed`, `hospital_patient`, `assigned_from`, `assigned_until`) VALUES 
			('".$num1."','".htmlentities(trim(addslashes(strip_tags($hospital_bed))))."',
					'".htmlentities(trim(addslashes(strip_tags($hospital_patient_id))))."','".htmlentities(trim(addslashes(strip_tags($date))))."',NULL)");	
		}
		
		}
		
		
		
		if($_GET["patient_bed"]=="transfer" && $_GET['patient_id'] != NULL && $_GET['transfer_bed_id'] != NULL)
		{
						$patient_id = $_GET['patient_id'];
						$assigned_bed_id = $_GET['assigned_bed_id'];
						$transfer_bed_id = $_GET['transfer_bed_id'];
		
		$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` ");
		$num1 = $sql->rowCount();
		$num1++;
						 
		 $sql = $link->query("UPDATE `rc_hospital_patient_bed_assign` SET `assigned_until`='".htmlentities(trim(addslashes(strip_tags($date))))."' WHERE `hospital_patient`='".$patient_id."' AND `assigned_until` IS NULL ");
		$sql->execute();
		if( $_GET['transfer_bed_id'] != 0)
		{
		$sql = $link->query("INSERT INTO `rc_hospital_patient_bed_assign`(`patient_bed_assign_id`, `hospital_bed`, `hospital_patient`, `assigned_from`, `assigned_until`) VALUES 
			('".$num1."','".htmlentities(trim(addslashes(strip_tags($transfer_bed_id))))."',
					'".htmlentities(trim(addslashes(strip_tags($patient_id))))."','".htmlentities(trim(addslashes(strip_tags($date))))."',NULL)");	
			
			$sql = $link->query("UPDATE `rc_hospital_patient` SET `is_assigned`='1' WHERE `hospital_patient_id`='".$patient_id."' ");
			$sql->execute();
		}
			else
			{
				$sql = $link->query("UPDATE `rc_hospital_patient` SET  `is_deactivated`='1' ,`is_assigned`='0' WHERE `hospital_patient_id`='".$patient_id."' ");
				$sql->execute();
			}
		if($sql)
		{
			echo "Patient Bed Transfer Successful";
		}
		}
		
		
		
		if($_GET["patient_bed"]=="check_out" && $_GET['bed_id'] != NULL)
		{
						$bed_id = $_GET['bed_id'];
						
			$sql = $link->query("SELECT * FROM `rc_hospital_patient_bed_assign` WHERE `hospital_bed` = '".$_GET["bed_id"]."' AND `assigned_until` IS NULL ORDER BY `patient_bed_assign_id` DESC LIMIT 0,1");
			 $rows = $sql->fetch();
			
			 $patient_id = $rows['hospital_patient'];
							 
		 $sql = $link->query("UPDATE `rc_hospital_patient_bed_assign` SET `assigned_until`='".htmlentities(trim(addslashes(strip_tags($date))))."' WHERE `hospital_bed`='".$bed_id."' AND `assigned_until` IS NULL ");
		$sql->execute();
		 
		 $sql = $link->query("UPDATE `rc_hospital_patient` SET `is_deactivated`='1' ,`is_assigned`='0' WHERE `hospital_patient_id`='".$patient_id."' ");
		 $sql->execute();
		if($sql)
		{
			echo "Patient Check-Out Successful";
		}
		
		}
		
		
		
//------------------------------------------------------------------------- BED-DEVICE ASSIGNMENT  ---------------------------------------------------->
						
						
			$cont = 1;
			$i=0;
	
				 $sql = $link->query("SELECT * FROM `rc_hospital_devices` WHERE umbrella='".$umbrella."' AND hospital='".$hospital."' AND hospital_location='".$hospital_location."'");	 
								 while($rows = $sql->fetch())
									{
										$all_devices[] = [
							"device_id"   			  => $rows['hospital_device_id'],
							"device_name"   		  => $rows['device_name'],
							"active"   			 	  => $rows['is_deactivated'],
							"is_assigned"   		  => $rows['is_assigned'],
							];
									}
									
									
			 $sql = $link->query("SELECT 
			rhla.hospital_area_id hospital_location_id,
			rhla1.area_name area_name,
			rhla.area_name hospital_location,
			rhb.hospital_bed_id bed_id,
			rhb.bed_number bed_number,
			
			rhdb.hospital_device device_id,
			rhd.device_name device_name,
			rhd.is_deactivated is_deactivated,
			rhd.is_assigned is_assigned,
			rhdb.assigned_from assigned_from,
			rhdb.assigned_until assigned_until
			
			FROM   rc_hospital_location_areas rhla1
			LEFT JOIN rc_hospital_location_areas rhla ON rhla1.lft_ref < rhla.lft_ref AND rhla1.rgt_ref > rhla.rgt_ref AND rhla.level_number=1 AND  rhla.hospital='".$hospital."' AND rhla.umbrella='".$umbrella."' 
			LEFT JOIN rc_hospital_beds rhb ON rhb.hospital_location_area = rhla.hospital_area_id
			LEFT JOIN rc_hospital_device_bed_assign rhdb ON rhb.hospital_bed_id = rhdb.hospital_bed AND rhdb.assigned_until IS NULL
			LEFT JOIN rc_hospital_devices rhd ON rhdb.hospital_device = rhd.hospital_device_id
			WHERE   rhla1.hospital='".$hospital."' AND rhla1.umbrella='".$umbrella."' AND rhla1.level_number=0 ORDER BY rhb.hospital_bed_id");	 
								 while($rows = $sql->fetch())
									{				
										
											$json_device[] = [
												"device_id"   			  => $rows['device_id'],
												"device_name"   		  => $rows['device_name'],
												"active"   			 	  => $rows['is_deactivated'],
												"is_assigned"   		  => $rows['is_assigned']
												];
										
												
											$json_bed[$i] = [
												"area_master"    	 		      => $rows['area_name'],
												"area"    	 		      => $rows['hospital_location'],
												"bed_id"    			  => $rows['bed_id'],
												"bed_number"   			  => $rows['bed_number'],
												"devices"				  => $json_device
												];
												
											$json_area_master[] = [
												"area"    	 			 => $rows['area_name']
												];
										
												unset($json_device);
												$i++;
									}
				
								
						for($i=0; $i<count($areas);$i++) 
						{
							for($j=0; $j<count($json_bed);$j++) 
								{
									if($areas[$i]['area'] == $json_bed[$j]['area'])
									{
										unset($json_bed[$j]['area']);
										unset($json_bed[$j]['area_master']);
												$areas[$i]['area_master'] = $json_area_master[$j]['area'];
												$areas[$i]['beds'][] = $json_bed[$j];
									}
								}
						}			
						$bedData = json_encode($areas, JSON_PRETTY_PRINT);					 
	
 
 //------------------------------------------------------------------------- ---------------------------------------------------->

	
		extract($_POST);
		
		if($_GET["bed"]=="show")
		{
						echo $bedData;
		}
		
		if($_GET["device"]=="show")
		{
						echo json_encode($all_devices, JSON_PRETTY_PRINT);
		}
		
		
		if($_GET['area']=="show")
		{
					 //------------------------------------------------------------------------- AREAS  ---------------------------------------------------->
			$cont = 1;
		 $sql = $link->query("SELECT 
			rhla.hospital_area_id hospital_area_id,
			rhla1.area_name area_name,
			rhla.area_name area_name1
			
			FROM   rc_hospital_location_areas rhla1
			LEFT JOIN rc_hospital_location_areas rhla ON rhla1.lft_ref < rhla.lft_ref AND rhla1.rgt_ref > rhla.rgt_ref AND rhla.level_number=1 AND  rhla.hospital='".$hospital."' AND rhla.umbrella='".$umbrella."'
			WHERE   rhla1.hospital='".$hospital."' AND rhla1.umbrella='".$umbrella."' AND rhla1.level_number=0 ");
		 while($rows = $sql->fetch())
				{
					$area[1]['area'] = $rows['area_name'];
                        $area[] = [
							"area"		     	  => $rows['area_name1']						
							];
							$cont++;
						}
			echo json_encode($area, JSON_PRETTY_PRINT);
		}
		
 //------------------------------------------------------------------------- BEDS-DEVICE UPDATE  ---------------------------------------------------->

if($_GET['bed']=="update")
		{
						$device_id = $_GET['device_id'];
						$bed_id = $_GET['bed_id'];
						$area_id = $_GET['area_id'];

						 $sql = $link->query("SELECT * FROM `rc_hospital_device_bed_assign` ");
						 $num = $sql->rowCount();
						 $num++;
						 
						 $sql = $link->query("SELECT * FROM `rc_hospital_device_bed_assign` WHERE `umbrella` = '".$umbrella."' AND `hospital` = '".$hospital."' AND `hospital_bed` ='".$bed_id."'  AND `assigned_until` IS NULL   ");
						 $rows = $sql->fetch();

						echo  $hospital_device = $rows['hospital_device'];
		
			
		
				if($device_id != NULL)
				{	
					$sql = $link->query("INSERT INTO `rc_hospital_device_bed_assign`(`device_bed_assign_id`,`umbrella`, `hospital`, `hospital_location`, `hospital_area`, `hospital_bed`, `hospital_device`, `assigned_from`, `assigned_until`) VALUES 
					('".$num."','".$umbrella."','".$hospital."','".$hospital_location."','".htmlentities(trim(addslashes(strip_tags($area_id))))."',
											'".htmlentities(trim(addslashes(strip_tags($bed_id))))."',	
											'".htmlentities(trim(addslashes(strip_tags($device_id))))."',
											'".htmlentities(trim(addslashes(strip_tags($date))))."',NULL)");	
					$sql = $link->query("UPDATE `rc_hospital_devices` SET `is_assigned`=1  WHERE `hospital_device_id`='".$device_id."' AND `umbrella`='".$umbrella."' AND `hospital`='".$hospital."'");
					$sql->execute();
				}
		
		
			$sql = $link->query("UPDATE `rc_hospital_device_bed_assign` SET `assigned_until`='".$date."' WHERE `umbrella`='".$umbrella."' AND `hospital`='".$hospital."'  AND `hospital_device` ='".$hospital_device."' AND `assigned_until`IS NULL   ");
			$sql->execute();
			$sql = $link->query("UPDATE `rc_hospital_devices` SET `is_assigned`=0  WHERE `hospital_device_id`='".$hospital_device."' AND `umbrella`='".$umbrella."' AND `hospital`='".$hospital."'");	
			$sql->execute();
			if($sql)
				{		
						echo "Successfully Saved";
				}           
		}
		
	if($_GET['device']=="update")
		{
						$device_id = $_GET['device_id'];
						$up_active = $_GET['up_active'];
					if($up_active == 'true')
						{
							$up_active =1;
						}	
					else
					{
						$up_active =0;
					}
					
			$sql = $link->query("UPDATE `rc_hospital_devices` SET `is_deactivated`='".$up_active."'  WHERE `hospital_device_id`='".$device_id."' AND `umbrella`='".$umbrella."' AND `hospital`='".$hospital."'");	
			$sql->execute();
					if($sql)
						{		
								echo "Successfully Saved";
						}           			
		}
 
 
 //------------------------------------------------------------------------- STATS  ---------------------------------------------------->

 
 
		if($_GET['bed']=="total")
			{
					 $sql = $link->query("SELECT * FROM rc_hospital_beds WHERE umbrella='".$umbrella."' AND hospital='".$hospital."' ");	 
					 $rows = $sql->rowCount();
					 echo $rows;						
			}
			
		if($_GET['device']=="total")
			{
					 $sql = $link->query("SELECT * FROM `rc_hospital_devices` WHERE umbrella='".$umbrella."' AND hospital='".$hospital."' AND hospital_location='".$hospital_location."'");	 
					 $rows = $sql->rowCount();
					 echo $rows;				
			}
		
		if($_GET['device']=="active")
			{
					 $sql = $link->query("SELECT * FROM `rc_hospital_devices` WHERE umbrella='".$umbrella."' AND hospital='".$hospital."' AND hospital_location='".$hospital_location."' AND is_deactivated=1");	 
					 $rows = $sql->rowCount();
					 
					 echo $rows;				
			}
		
		if($_GET['device']=="availabel")
			{
					 $sql = $link->query("SELECT * FROM `rc_hospital_devices` WHERE umbrella='".$umbrella."' AND hospital='".$hospital."' AND hospital_location='".$hospital_location."' AND `is_assigned`=0");	 
					 $rows = $sql->rowCount();
					 echo $rows;			
			}
			
			
			
			