
var app = angular.module('myApp', []);
angular.module('app', ['ngRoute']);

var baseURL = "../php/fetch.php?";
app.controller('bed_device_mapping', function($scope,$window, $http, $timeout) {
    
	$scope.login_credential = {
      username: 'MHENSSS'
    };
		
	$http.get(baseURL+"bed=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.device_bed_mapping = response.data;       
		console.log($scope.device_bed_mapping);
		
	});	
	
	$http.get(baseURL+"device=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.devices = response.data;       
		console.log($scope.devices);
		
	});
	
	$http.get(baseURL+"bed=total&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.bed_total = response.data;       
		
	});	
	$http.get(baseURL+"device=total&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.device_total = response.data;       
		
	});	
	$http.get(baseURL+"device=active&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.active_device = response.data;       
		
	});	
	$http.get(baseURL+"device=availabel&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.availabel_device = response.data;       
		
	});	
	
	 $scope.passwordData = {
            pass: ''
        };
	$scope.orderByMe = function(header_name) {
        $scope.myOrderBy = header_name;
	 };
	
	 $scope.change = function() {
		   
		if($scope.passwordData.pass == 1234)
		{
			$scope.normalView = false;
			$scope.editView = true;
			$scope.popVerify = true;
			$scope.msg="";			
		}
		else
		{
			$scope.msg = "Incorrect Password. Try again.";				
		}	
	
	 };
	  
		 
	 $scope.update_bed = function(device_id,bed_id,area_id) {	
	 $http.get(baseURL+"bed=update&device_id="+device_id+"&bed_id="+bed_id+"&area_id="+area_id+"&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.update_bed_reply = response.data;
		console.log(baseURL+"bed=update&device_id="+device_id+"&bed_id="+bed_id+"&area_id="+area_id+"&username="+$scope.login_credential.username);
			
				$http.get(baseURL+"device=show&username="+$scope.login_credential.username)
			   .then(function(response) {
					$scope.devices = response.data;       
					console.log($scope.devices);		
				});
	
				$http.get(baseURL+"bed=show&username="+$scope.login_credential.username)
				.then(function(response) {
					$scope.device_bed_mapping = response.data;       
					console.log($scope.device_bed_mapping);
					
				});	
	
				$http.get(baseURL+"device=availabel&username="+$scope.login_credential.username)
				.then(function(response) {
					$scope.availabel_device = response.data;       
		
	});	
  });	
};
	 
	 
	 
	 $scope.update_active = function(up_active,device_id) {	 
	$http.get(baseURL+"device=update&device_id="+device_id+"&up_active="+up_active+"&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.update_active_reply = response.data;	
	console.log(baseURL+"device=update&device_id="+device_id+"&up_active="+up_active+"&username="+$scope.login_credential.username);		
		
		$http.get(baseURL+"device=active&username="+$scope.login_credential.username)
				.then(function(response) {
					$scope.active_device = response.data;       
				});	
		});	 
	 };	 
	  
	  
	 $scope.update = function() {
		location.reload();	 
		};
	 
	 
	$http.get(baseURL+"area=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.areas = response.data;
		console.log($scope.areas);
		
	 
	});		
		
	
})


app.controller('patient_bed_mapping', function($scope,$window, $http, $timeout) {
    
		$scope.login_credential = {
      username: 'MHENSSS'
    };
	
	$http.get(baseURL+"patient_bed=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.patient_beds = response.data;       
		console.log($scope.patient_beds);
		
	});	
	 $scope.check_in = {
      patient_name: '',
      patient_number: '',
      bed_id: ''
    };
	
	 $scope.assign_bed = function(patient_name,patient_number,bed_id) {	
	 $scope.random = Math.round(Math.random() * (999999 - 1) + 1);
	 $http.get(baseURL+"patient_bed=check_in&patient_name="+patient_name+"&patient_number="+patient_number+"&bed_id="+bed_id+"&username="+$scope.login_credential.username+"&random="+$scope.random)
    .then(function(response) {
        $scope.assign_bed_reply = response.data;
		console.log($scope.assign_bed_reply);
	 $scope.master = {patient_name:"", patient_number:"", bed_id:""};

        $scope.check_in = angular.copy($scope.master);
		
		$http.get(baseURL+"patient_bed=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.patient_beds = response.data;       
		console.log($scope.patient_beds);
		
	});	
			
	  });	
	  
	  
	};


	$scope.active_check_in_view = function() {	

	$scope.check_in_view = true; 
	$scope.check_out_view = false;
	$scope.transfer_view = false;
};

	$scope.active_check_out_view = function() {	
	
	$scope.check_in_view = false; 
	$scope.check_out_view = true;
	$scope.transfer_view = false;
	
	$http.get(baseURL+"patient_bed_details=show")
    .then(function(response) {
        $scope.patient_bed_details = response.data;
		console.log($scope.patient_bed_details);
			
	  });
};

	$scope.active_transfer_view = function() {	

	$scope.check_in_view = false; 
	$scope.check_out_view = false;
	$scope.transfer_view = true;
	
	$http.get(baseURL+"patient_details1=show")
    .then(function(response) {
        $scope.patient_details1 = response.data;
		console.log($scope.patient_details1);
			
	  });
};

	$scope.fetch_patient = function(bed_id) {	
	$http.get(baseURL+"patient=fetch_patient_number&bed_id="+bed_id+"&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.fetch_patient_number = response.data;
		console.log($scope.fetch_patient_number);
			
	  });	
	$http.get(baseURL+"patient=fetch_hospital_bed&bed_id="+bed_id+"&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.fetch_patient_name = response.data;
		console.log($scope.fetch_patient_name);
			
	  });
};
	$scope.fetch_patient1 = function(bed_id) {	
	$http.get(baseURL+"patient=fetch_patient_number1&bed_id="+bed_id)
    .then(function(response) {
        $scope.fetch_patient_number1 = response.data;
		console.log($scope.fetch_patient_number1);
			
	  });	
	$http.get(baseURL+"patient=fetch_patient_id1&bed_id="+bed_id)
    .then(function(response) {
        $scope.fetch_patient_id1 = response.data;
		console.log($scope.fetch_patient_id1);
			
	  });	
	$http.get(baseURL+"patient=fetch_hospital_bed1&bed_id="+bed_id)
    .then(function(response) {
        $scope.fetch_patient_name1 = response.data;
		console.log($scope.fetch_patient_name1);
			
	  });
	};
	 $scope.transfer = {
		  patient_id: '',
		  bed_id: '',
		  transfer_bed_id: ''
		};
	$scope.transfer_bed = function(patient_id,assigned_bed_id,transfer_bed_id) {	
	 $http.get(baseURL+"patient_bed=transfer&patient_id="+patient_id+"&assigned_bed_id="+assigned_bed_id+"&transfer_bed_id="+transfer_bed_id)
    .then(function(response) {
        $scope.transfer_bed_reply = response.data;
		console.log(baseURL+"patient_bed=transfer&patient_id="+patient_id+"&assigned_bed_id="+assigned_bed_id+"&transfer_bed_id="+transfer_bed_id);
		alert($scope.transfer_bed_reply);
		location.reload();		
	  });	
	};
	
	$scope.check_out_bed = function(bed_id) {	
	 $http.get(baseURL+"patient_bed=check_out&bed_id="+bed_id)
    .then(function(response) {
        $scope.check_out_bed_reply = response.data;
		console.log($scope.check_out_bed_reply);
	 
		alert($scope.check_out_bed_reply);
		location.reload();		
	  });	
	};
	
	$http.get(baseURL+"device=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.devices = response.data;       
		console.log($scope.devices);
		
	});
	
})


	
app.controller('device_mapping', function($scope,$window, $http, $timeout) {
    
		$scope.login_credential = {
      username: 'MHENSSS'
    };
	
	
	
	$http.get(baseURL+"device=show&username="+$scope.login_credential.username)
    .then(function(response) {
        $scope.devices = response.data;       
		console.log($scope.devices);
		
	});
	
});


	





 











